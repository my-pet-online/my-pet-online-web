[![pipeline status](https://gitlab.com/my-pet-online/my-pet-online-web/badges/master/pipeline.svg)](https://gitlab.com/my-pet-online/my-pet-online-web/commits/master)

## My Pet Online

My Pet Online is an online space for your dearest pet.

### Installation

To start local server:

    $ yarn start

To run the tests:

    $ yarn test

For more options just follow the original readme generated with `create-react-app`.
