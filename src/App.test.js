import React from 'react'
import ReactDOM from 'react-dom'
import renderer from 'react-test-renderer'
import App from './App'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<App signedIn />, div)
})

it('renders App correctly', () => {
  const tree = renderer
    .create(<App signedIn />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
