import React, { Component } from 'react'
import firebase from 'firebase'
import { FirebaseAuth } from 'react-firebaseui'
import logo from './logo.svg'
import './App.css'

class App extends Component {
  constructor (props) {
    super(props)
    const { signedIn } = props
    this.state = {
      signedIn: signedIn || false
    }

    this.uiConfig = {
      // Popup signin flow rather than redirect flow.
      signInFlow: 'popup',
      // We will display Google and Facebook as auth providers.
      signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        firebase.auth.GithubAuthProvider.PROVIDER_ID
      ],
      // Sets the `signedIn` state property to `true` once signed in.
      callbacks: {
        signInSuccess: () => {
          this.setState({signedIn: true})
          console.log('signed in user:', firebase.auth().currentUser)
          return false // Avoid redirects after sign-in.
        }
      }
    }
  }

  async signOut () {
    await firebase.auth().signOut()
    console.log('User signed out correctly!')
    console.log('signed in user:', firebase.auth().currentUser)
    this.setState({
      signedIn: false
    })
  }

  render () {
    return (
      <div className='App'>
        <header className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h1 className='App-title'>Welcome to React</h1>
        </header>
        <p className='App-intro'>
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        {
          this.state.signedIn
          ? <div>
            <p>Hey! You are signed in ;)</p>
            <p>But you can sign out</p>
            <button onClick={() => this.signOut()}>Sign out</button>
          </div>
          : <FirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
        }
      </div>
    )
  }
}

export default App
