import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

import firebase from 'firebase'

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyBJhtXgNApoUQZWxZzKjAwgwNo7XC8Mais',
  authDomain: 'my-pet-online-ep.firebaseapp.com',
  databaseURL: 'https://my-pet-online-ep.firebaseio.com',
  projectId: 'my-pet-online-ep',
  storageBucket: 'my-pet-online-ep.appspot.com',
  messagingSenderId: '98112023234'
}
firebase.initializeApp(config)

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
